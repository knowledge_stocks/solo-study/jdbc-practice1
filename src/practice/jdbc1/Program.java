package practice.jdbc1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import practice.jdbc1.service.Constants;

public class Program {

	public static void main(String[] args) {
		
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			Class.forName(Constants.DB_DRIVER);
			conn = DriverManager.getConnection(Constants.DB_URL, Constants.DB_USER, Constants.DB_PWD);
			
			String sql = "SELECT * FROM NOTICE";
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			
			if(rs.next()) {
				int id = rs.getInt("ID");
				String title = rs.getString("TITLE");
				String writerId = rs.getString("WRITER_ID");
				String content = rs.getString("CONTENT");
				Timestamp regDate = rs.getTimestamp("REGDATE");
				int hit = rs.getInt("HIT");
				String files = rs.getString("FILES");
				
				System.out.printf("id: %d\n", id);
				System.out.printf("title: %s\n", title);
				System.out.printf("writerId: %s\n", writerId);
				System.out.printf("content: %s\n", content);
				System.out.printf("regDate: %s\n", regDate);
				System.out.printf("hit: %d\n", hit);
				System.out.printf("files: %s\n", files);
			}
			
			rs.close();
			st.close();
			conn.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

}
